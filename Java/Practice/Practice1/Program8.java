/*
 * Given an array of integers nums and an integer target, return index of the two 
 * numbers such that they add up to the target.
 * Input: 
 * array = [3,6,16,15], target = 9
 * Output: 
 * [0,1]
 */

import java.io.*;
class Question8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array elements are : ");
		for(int x : arr){
			System.out.print(x + " ");
		}
		System.out.println();

		System.out.println("Enter the target number : ");
		int target = Integer.parseInt(br.readLine());

		for(int i = 0; i< arr.length; i++){
			for(int j=i; j<arr.length; j++){
				if(i == j){
					continue;
				}
				if((arr[i]+arr[j]) == target){
					System.out.println("[" + i + "," + j + "]");
				}
			}
		}
	}
}



