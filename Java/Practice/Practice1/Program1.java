/*
 * Write a Program to Print following Pattern.
 * note: take rows from user.
 * A C E G
 * B D F
 * C E
 * D
 */

import java.io.*;
class Question1{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number of rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
			int ch = 64 + i;
			for(int j=1; j<=rows-i+1; j++){
				System.out.print((char)ch + " ");
				ch = ch + 2;
			}
			System.out.println();
		}
	}
}
