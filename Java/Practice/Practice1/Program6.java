/*
 * write a Program to reverse a given String.
 * Input String:
 * Ajay Bhosle
 * Output String:
 * elsohB yajA
 */

import java.io.*;
class Question6{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the string that is to be reversed : ");
		String str = br.readLine();
		
		char arr[] = str.toCharArray();
		String str1 = new String();
		char ch;

		for(int i=0; i<arr.length; i++){
			ch = arr[i];
			str1 = ch + str1;
		}
		System.out.println("Reversed string = " + str1);
	}
}
