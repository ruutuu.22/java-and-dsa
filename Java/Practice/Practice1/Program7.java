/*
 * Write a Program that takes a number as input from the user
 * and prints only those digits from that number, which are perfect
 * divisors of the actual number.
 * Input: 124
 * Output: The Perfect Divisor Digits from the Number 124 are 1 2 4
 */

import java.io.*;
class Question7{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number : ");
		int num = Integer.parseInt(br.readLine());
		
		System.out.print("The perfect divisor digits from the number " + num + " are " );
		int rev = 0;
		for(int i = num; i!=0; i=i/10){
			rev = (rev*10) + (i%10);
		}
		for(int i = rev; i!=0; i=i/10){
			int rem =i%10;
			if(num%rem == 0){
				System.out.print(rem + " ");
			}
		}
		System.out.println();
	}
}
