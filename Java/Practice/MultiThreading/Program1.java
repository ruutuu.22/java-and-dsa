/*
 * Example - 1
 */

class MyThread extends Thread{
	MyThread(ThreadGroup tg, String str){
		super(tg, str);
	}
	public void run(){
		System.out.println(getName());
		System.out.println(getThreadGroup().getName());
	}
}

class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup tG = new ThreadGroup("C2W");

		MyThread obj1 = new MyThread(tG, "AI");

		MyThread obj2 = new MyThread(tG, "ML");

		obj1.start();
		obj2.start();
	}
}

