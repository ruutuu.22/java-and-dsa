/*
 * # = = = =
 * = # = = =
 * = = # = =
 * = = = # =
 * = = = = #
 */

import java.io.*;
class Question2{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				if(i == j){
					System.out.print("# ");
				}else{
					System.out.print("= ");
				}
			}
			System.out.println();
		}
	}
}

