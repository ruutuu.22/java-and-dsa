/*
 * WAP to take a number as input and print the addition of factorials of each digit from that number
 */

import java.io.*;
class Question9{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number = ");
		int num = Integer.parseInt(br.readLine());
		int sum = 0;
		for(int temp=num; temp!=0; temp=temp/10){
			int rem = temp%10;
			int fact = 1;
			for(int i=1; i<=rem; i++){
				fact = fact*i;
			}
			sum = sum + fact;
		}
		System.out.println("Addition of factorials of each digit of " + num + " = " + sum);
	}
}
			
