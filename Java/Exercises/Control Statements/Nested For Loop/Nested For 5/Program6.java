/*
 * WAP and take tewo characters if these characters are equal then print them as it is but if 
 * they are unequal then print their difference.
 */

import java.io.*;
import java.util.*;
class Question6{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter two alphabets : ");
		String alpha = br.readLine();
		
		StringTokenizer st = new StringTokenizer(alpha, " ");
		if(st.countTokens() == 1 || st.countTokens() >=3){
			System.out.println("Please enter two characters.");
		}else{
			String Token1 = st.nextToken();
			char alpha1 = Token1.charAt(0);
			String Token2 = st.nextToken();
			char alpha2 = Token2.charAt(0);
	
			if(alpha1 == alpha2){
				System.out.println(alpha1);
			}else if(alpha1 > alpha2){
				if((alpha1 - alpha2) <= 26){
					System.out.println((alpha1 - alpha2));
				}else{
					System.out.println("Uppercase characters cannot be compared with lowercase characters.");
				}
			}else{
				if((alpha2 - alpha1) <= 26){
					System.out.println((alpha2 - alpha1));
				}else{
					System.out.println("Uppercase characters cannot be compared with lowercase characters.");
				}
			}
		}
	}
}
