/*
 * rows = 5
 * O
 * 14	13
 * L	K	J
 * 9	8	7	6
 * E	D	C	B	A
 *
 * rows = 4
 * 10
 * I	H
 * 7	6	5
 * D	C	B	A
 */

import java.io.*;
class Question7{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows = ");
		int rows = Integer.parseInt(br.readLine());
		
		System.out.println();
		int num = rows*(rows+1)/2;
		int ch = 64 + num;

		for(int i=1; i<=rows; i++){
			for(int j=1; j<=i; j++){
				if(rows%2 == 0){
					if(i%2 == 0){
						System.out.print(((char)ch) +""+ "	");
					}else{
						System.out.print(num + "	");
					}
				}else{
					if(i%2 == 0){
						System.out.print(num + "	");
					}else{
						System.out.print(((char)ch) +""+ "	");
					}
				}
				num--;
				ch--;
			}
			System.out.println();
		}
	}
}

