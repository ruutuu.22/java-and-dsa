/*
 * 5 4 3 2 1 
 * 8 6 4 2
 * 9 6 3
 * 8 4 
 * 5
 */

import java.io.*;
class Question3{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int num = rows - i + 1;
			for(int j=1; j<= rows-i+1;j++){
				int ans = num * i;
				System.out.print(ans + " ");
				num--;
			}
			System.out.println();
		}
	}
}

