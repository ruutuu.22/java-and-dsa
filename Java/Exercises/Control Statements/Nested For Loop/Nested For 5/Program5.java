/*
 * 0
 * 1 1
 * 2 3  5
 * 8 13 21 34
 */

import java.io.*;
class Question5{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		int x = 0;
		int y = 1;
		int z = 0;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=i; j++){
				z=x+z;
				System.out.print(z + "	");
				x=y;
				y=z;
			}
			System.out.println();
		}
	}
}

