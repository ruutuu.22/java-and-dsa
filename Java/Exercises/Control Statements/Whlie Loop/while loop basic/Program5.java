/*
 * Program - 5
 * WAP tp print the square of even digits of the given number.
 */

class Question5{
	public static void main(String[] rutu){
		int num = 987456321;
		int count = 0;
		while(num != 0){
			if((num%10)%2 == 0){
				System.out.print(((num%10) * (num%10)) + " ");
			}
			num = num/10;
		}
		System.out.println();
	}
}
