/*
 * Program - 10
 * WAP to check whether the number is palindrome number or not
 */

class Question10{
	public static void main(String[] rutu){
		int num = 987454789;
		int temp = num;
		int rev = 0;
		while(temp != 0){
			int rem = temp%10;
			rev = (rev*10) + rem;
			temp = temp/10;
		}
		if(num == rev){
			System.out.println(num + " is a palindrome number");
		}else{
			System.out.println(num + " is not a palindrome number");
		}
	}
}
