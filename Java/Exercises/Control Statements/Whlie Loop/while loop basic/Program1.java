/*
 * Program - 1
 * WAP to print a table of 2
 */

class Question1{
	public static void main(String[] rutu){
		int num = 2;
		int i = 1;
		while(i<=10){
			System.out.print((num * i) + " ");
			i++;
		}
		System.out.println();
	}
}
