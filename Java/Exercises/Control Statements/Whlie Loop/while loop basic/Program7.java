/*
 * Program - 7
 * WAP
 *
 * Input = 6
 * Output = 6 5 4 3 2 1 
 *
 * Input = 7
 * Output = 7 5 3 1
 */

class Question7{
	public static void main(String[] rutu){
		int num = 6;
		if(num%2 == 0){
			while(num != 0){
				System.out.print(num + " ");
				num--;
			}
		}else{
			while(num >= 0){
				System.out.print(num + " ");
				num = num - 2;
			}
		}
		System.out.println();
	}
}
