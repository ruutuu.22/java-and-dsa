/*
 * Program - 2
 * WAP to take size of array from user and also take integer elements from user and
 * print the product of even element only
 */

import java.io.*;
class Question2{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int product = 1;
		
		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 == 0){
				product = product * arr[i];
			}
		}
		System.out.println("Product of even elements in array is " + product);
	}
}


