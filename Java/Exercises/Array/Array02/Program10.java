/*
 * Program - 10
 * WAP to print the elements whose addition of digits is even.
 */

import java.io.*;
class Question10{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.print("Elements with even digit addtion from array are : ");
		for(int i=0; i<arr.length; i++){
			int sum = 0;
			for(int temp = arr[i]; temp!=0; temp=temp/10){
				sum = sum + (temp%10);
			}
			if(sum%2 == 0){
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}
}
