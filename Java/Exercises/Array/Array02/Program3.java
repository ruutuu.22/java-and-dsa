/*
 * Program - 3
 * WAP to find the sum of even and odd numbers in an array.
 * Display sum value.
 */

import java.io.*;
class Question3{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		int even_sum = 0;
		int odd_sum = 0;

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 == 0){
				even_sum = even_sum + arr[i];
			}else{
				odd_sum = odd_sum + arr[i];
			}
		}
		System.out.println("Sum of even array elements is " + even_sum);
		System.out.println("Sum of odd array elements is " + odd_sum);
	}
}
