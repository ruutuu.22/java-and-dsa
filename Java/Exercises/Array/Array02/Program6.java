/*
 * Program - 6
 * WAP to take size of array from user and also take integer elements from user
 * Find the maximum element from the array
 */

import java.io.*;
class Question6{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int max = arr[0];
		for(int i=0; i<arr.length; i++){
			if(max < arr[i]){
				max = arr[i];
			}
		}
		System.out.println("Maximum element of array is " + max);
	}
}
