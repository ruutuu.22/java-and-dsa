/*
 * Unary Operator - 4
 */

class UnaryOp4{
	public static void main(String[] rutu){
		int x = 10;
		int y = 20;

		int ans = ++x + y++ + x++;

		System.out.println(x);
		System.out.println(y);
		System.out.println(ans);
	}
}

