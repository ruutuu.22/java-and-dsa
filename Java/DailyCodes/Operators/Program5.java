/*
 * Unary Operator - 1
 */

class UnaryOp1{
	public static void main(String[] rutu){
		int x = 5;
		int y = 7;

		
		System.out.println(++x);
		System.out.println(++y);

		System.out.println(--x);
		System.out.println(--y);

		System.out.println(x);
		System.out.println(y);
	}
}	
