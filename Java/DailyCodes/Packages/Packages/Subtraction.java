/*
 * package for subtraction
 */

package arithfun;

public class Subtraction{
	int num1 = 0;
	int num2 = 0;
	public Subtraction(int num1, int num2){
		this.num1 = num1;
		this.num2 = num2;
	}
	public int sub(){
		if(num1 >= num2){
			return num1-num2;
		}else{
			return num2-num1;
		}
	}
}
