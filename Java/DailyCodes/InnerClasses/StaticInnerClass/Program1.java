/*
 * Example - 1
 */

class Example1_Outer{
	static class Example1_Inner{
		void m1(){
			System.out.println("In m1");
		}
	}
}

class Example1_Client{
	public static void main(String[] args){
		Example1_Outer.Example1_Inner obj = new Example1_Outer.Example1_Inner();
		obj.m1();
	}
}
