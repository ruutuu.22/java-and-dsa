/*
 * Example - 1
 *
 * No other way to call methods from Methods Local Inner Class,
 * other than creating it's object in the same method where it is been written
 */

class Example1_Outer{
	void m1(){
		System.out.println("In m1 - Outer");
		class Example1_Inner{
			void m1(){
				System.out.println("In m2 - Inner");
			}
		}
		Example1_Inner obj = new Example1_Inner();
		obj.m1();
	}
	void m2(){
		System.out.println("In m2 - Outer");
	}

	public static void main(String[] args){
		Example1_Outer obj = new Example1_Outer();
		obj.m1();
		obj.m2();
	}
}

