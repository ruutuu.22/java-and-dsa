/*
 * Example - 2
 */

class Example2_Outer{
	class Example2_Inner{
		void m1(){
			System.out.println("In m1 - INNER");
		}
	}

	void m2(){
		System.out.println("In m2 - OUTER");
	}
}

class Example2_Client{
	public static void main(String[] args){
		Example2_Outer.Example2_Inner obj1 = new Example2_Outer().new Example2_Inner();
		obj1.m1();
	}
}
