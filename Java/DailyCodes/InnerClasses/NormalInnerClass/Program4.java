/*
 * Example - 4
 *
 * Static is not allowed in any other block except the class
 */

class Example4_Outer{
	int x = 10;
	static int y = 20;
	class Example4_Inner{
		int a = 10;
		static int b = 20;
		void m1(){
			System.out.println(a);
			System.out.println(b);
		}
	}
}

class Example4_Client{
	public static void main(String[] args){
		Example4_Outer obj = new Example4_Outer();
		Example4_Outer.Example4_Inner obj1 = obj.new Example4_Inner();
		obj1.m1();
	}
}
