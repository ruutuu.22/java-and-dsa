/*
 * Example - 1
 *
 * IOException
 */

import java.io.*;
class Example1{
	public static void main(String[] args){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();		//br.readLine() throws IOException.
	}
}
