/*
 * Example - 1
 *
 * ArrayIndexOutOfBoundsException
 */

class Example1{
	public static void main(String[] args){
		int arr[] = new int[]{10, 20, 30, 40, 50};
		for(int i=0; i<=arr.length; i++){		//Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 5
			System.out.println(arr[i]);
		}
	}
}
