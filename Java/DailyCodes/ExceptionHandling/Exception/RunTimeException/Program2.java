/*
 * Example - 2
 *
 * ArithmeticException
 */

class Example2{
	void m1(){
		System.out.println("In m1");
		System.out.println(10/0);		//Exception in thread "main" java.lang.ArithmeticException: / by zero
		m2();
	}
	void m2(){
		System.out.println("In m2");
	}
	public static void main(String[] args){
		System.out.println("Start Main");
		Example2 obj = new Example2();
		obj.m1();
		System.out.println("End Main");
	}
}
