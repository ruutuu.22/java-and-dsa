/*
 * Example - 4
 *
 * NumberFormatException
 */

import java.io.*;
class Example4{
	void m1() throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int num = Integer.parseInt(br.readLine());		//if input given is other than int it gives exception
									//Exception in thread "main" java.lang.NumberFormatException: For input string: "abc"
		System.out.println(num);
	}
	public static void main(String[] args) throws IOException{
		Example4 obj = new Example4();
		obj.m1();
	}
}
