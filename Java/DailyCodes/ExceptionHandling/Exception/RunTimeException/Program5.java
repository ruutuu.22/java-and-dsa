/*
 * Example - 5
 *
 * IOException
 */

import java.io.*;
class Example5{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		System.out.println(str);

		br.close();

		String str1 = br.readLine();		//Exception in thread "main" java.io.IOException: Stream closed
		System.out.println(str1);
	}
}
