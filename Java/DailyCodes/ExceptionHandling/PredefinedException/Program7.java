/*
 * Example - 7
 *
 * Finally block is always visited and is used to close all the connection created 
 * as direct end of execution of code may lead the connections to be open forever
 */

class Example7{
	void m1(){
		System.out.println("In m1");
	}
	void m2(){
		System.out.println("In m2");
	}
	public static void main(String[] args){
		Example7 obj = new Example7();
		obj.m1();

		obj = null;

		try{
			obj.m2();
		}catch(ArithmeticException obj1){
			System.out.println("Here");
		}finally{
			System.out.println("Connection Closed");
		}
		System.out.println("End main");
	}
}
