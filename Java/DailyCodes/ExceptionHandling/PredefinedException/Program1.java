/*
 * Example - 1
 */

class Example1{
	public static void main(String[] args){
		System.out.println("Start main");
		try{
			System.out.println(10/0);		//Risky code
		}catch(ArithmeticException obj){
			System.out.println("Exception Occured");//Handling Code
		}
		System.out.println("End main");
	}
}
