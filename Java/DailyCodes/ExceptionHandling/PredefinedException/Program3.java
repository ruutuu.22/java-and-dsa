/*
 * Example - 3
 */

class Example3{
	public static void main(String[] args){
		System.out.println("Start main");
		try{
			System.out.println(10/0);	
		}catch(ArithmeticException obj){
			System.out.println(obj.getMessage());
		}
		System.out.println("End main");
	}
}
