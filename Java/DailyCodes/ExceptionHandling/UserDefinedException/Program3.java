/*
 * Example - 3
 *
 * Real Time Example
 */

import java.util.Scanner;

class OversQuotaFinishedException extends RuntimeException{
	OversQuotaFinishedException(String msg){
		super(msg);
	}
}
class InvalidEntryException extends RuntimeException{
	InvalidEntryException(String msg){
		super(msg);
	}
}
class Example3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Format (ODI/T20) : ");
		String format = sc.nextLine();

		if(!format.equals("ODI") && !format.equals("T20")){
                        throw new InvalidEntryException("Invalid Input");
                }

		System.out.println("Enter number of his personal over bowler is going to bowl : ");
		int nOvers = sc.nextInt();
		if(format.equals("ODI") && nOvers > 10){
			throw new OversQuotaFinishedException("Only 10 overs can be bowled by particular bowler in ODI.");
		}
		if(format.equals("T20") && nOvers > 4){
			throw new OversQuotaFinishedException("Only 4 overs can be bowled by particular bowler in T20");
		}
		if(nOvers < 0){
			throw new InvalidEntryException("Invalid Input");
		}

		if(format.equals("ODI") && nOvers > 10){
                        System.out.println("Bowler can bowl " + (10 - nOvers) + " overs.");
                }
                if(format.equals("T20") && nOvers > 4){
                        System.out.println("Bowler can bowl " + (4 - nOvers) + " overs.");
                }
	}
}
