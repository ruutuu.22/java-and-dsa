/*
 * Example - 7
 */

class Example7{
	void fun(int x){
		System.out.println("In fun");
		System.out.println(x);
	}
	
	public static void main(String[] args){
		Example7 obj = new Example7();
		obj.fun(10);
		obj.fun(10.5f);
	}
}

