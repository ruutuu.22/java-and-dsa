/*
 * Example - 15
 */

class Example15{
	int fun(int x){
		int val = x + 50;
		return val;
	}

	public static void main(String[] args){
		Example15 obj = new Example15();
		int ret = obj.fun(10);
		System.out.println(ret);
	}
}
