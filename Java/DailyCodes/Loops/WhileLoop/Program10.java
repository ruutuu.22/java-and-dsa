/*
 * Printing product of digits of a num
 */

class Question7{
	public static void main(String[] rutu){
		int num = 6543;
		int product = 1;
		while(num != 0){
			int rem = num%10;
			product = product + rem;
			num = num/10;
		}
		System.out.println("Product of Digits is = " + product);
	}
}
