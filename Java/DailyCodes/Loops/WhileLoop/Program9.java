/*
 * Printing sum of digits of a num
 */

class Question6{
	public static void main(String[] rutu){
		int num = 6543;
		int sum = 0;
		while(num != 0){
			int rem = num%10;
			sum = sum + rem;
			num = num/10;
		}
		System.out.println("Sum of Digits is = " + sum);
	}
}
