/*
 * Reverse a number
 */

class Question9{
	public static void main(String[] rutu){
		int num = 123;
		int rev = 0;
		System.out.println("Entered number is " + num);

		while(num != 0){
			int rem = num%10;
			rev = rev*10 + rem;
			num = num/10;
		}
		System.out.println("Reversed Number is " +  rev);
	}
}
