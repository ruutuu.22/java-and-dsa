/*
 * Example - 11
 * 1 
 * 2 C
 * 4 E 6
 * 7 H 9 J
 */

class Example11{
	public static void main(String[] args){
		int rows = 4;
		int ch = 65;
		int num = 1;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=i; j++){
				if(j%2 != 0){
					System.out.print(num + "	");
				}else{
					System.out.print((char)ch + "	");
				}
				num++;
				ch++;
			}
			System.out.println();
		}
	}
}
