/*
 * Example - 5
 * A B C D
 * A B C
 * A B
 * A
 */

class Example6{
	public static void main(String[] args){
		int row = 4;
		for(int i=1; i<=row; i++){
			int ch = 65;
			for(int j=1; j<=row-i+1; j++){
				System.out.print((char)ch + " ");
				ch++;
			}
			System.out.println();
		}
	}
}
