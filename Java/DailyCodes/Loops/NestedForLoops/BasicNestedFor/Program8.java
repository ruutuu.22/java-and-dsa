/*
 * Example - 8
 * A B C
 * A B C
 * A B C
 */

class Example8{
	public static void main(String[] args){
		int row = 3;
		int ch = 65;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
				System.out.print((char)ch + " ");
			}
			ch++;
			System.out.println();
		}
	}
}
