/*
 * Example - 2
 * 3 3 3 3
 * 3 3 3 3
 * 3 3 3 3
 */

class Example2{
	public static void main(String[] args){
		int n = 5;
		int row = 3;
		int col = 4;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=col; j++){
				System.out.print(n + " ");
			}
			System.out.println();
		}
	}
}
