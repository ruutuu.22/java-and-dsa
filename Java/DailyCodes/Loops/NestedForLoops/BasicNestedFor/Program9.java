/*
 * Example - 9
 * A 1 B 2
 * A 1 B 2
 * A 1 B 2
 * A 1 B 2
 */

class Example9{
	public static void main(String[] args){
		int row = 4;
		for(int i=1; i<=row; i++){
			int num = 1;
			int ch = 65;
			for(int j=1; j<=row; j++){
				if(j%2 == 0){
					System.out.print(num + " ");
					num++;
				}else{
					System.out.print((char)ch + " ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}

