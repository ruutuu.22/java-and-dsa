/*
 * Count of factors of the particular number
 */

class Question5{
	public static void main(String[] rutu){
		int N = 6;
		int count = 0;
		for(int i=1; i<= N; i++){
			if(N%i == 0){
				count++;
			}
		}
		System.out.println("Count of factors of " + N + " is " + count);
	}
}
