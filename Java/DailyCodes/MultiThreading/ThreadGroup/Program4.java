/*
 * Example - 4
 */

class MyThread_Example4 implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo_Example4{
	public static void main(String[] args) throws InterruptedException{
		ThreadGroup parentTG = new ThreadGroup("India");

		MyThread_Example4 obj1 = new MyThread_Example4();
		MyThread_Example4 obj2 = new MyThread_Example4();

		Thread t1 = new Thread(parentTG, obj1, "Maharashtra");
		Thread t2 = new Thread(parentTG, obj2, "Goa");

		t1.start();

		t2.start();
	}
}
