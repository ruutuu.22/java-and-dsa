/*
 * Example - 4
 *
 * Creating Child thread using Runnable Interface.
 * Makes it possible to inherit methods from other classes except from Thread class.
 */

class MyThread_Example4 implements Runnable{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}
}
class ThreadDemo_Example4{
	public static void main(String[] args){
		MyThread_Example4 obj = new MyThread_Example4();
		Thread t = new Thread(obj);
		t.start();

		System.out.println(Thread.currentThread().getName());
	}
}
