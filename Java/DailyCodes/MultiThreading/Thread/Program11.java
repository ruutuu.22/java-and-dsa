/*
 * Example - 11
 *
 * Concurrency Methods in Thread Class
 * join(long);
 * join(long, int);
 */

class MyThread_Example11 extends Thread{
	static Thread mnName = null;
	public void run(){
		for(int i=0; i<10; i++){
			try{
                        mnName.join(1000, 999999);
			/*
			 * @param  nanos
			 * 	{@code 0-999999} additional nanoseconds to wait
			 *
			 * @throws  IllegalArgumentException
			 * 	if the value of {@code millis} is negative, or the value
			 * 	of {@code nanos} is not in the range {@code 0-999999}
			 */
                	}catch(InterruptedException obj){
                	}
			System.out.println("In Thread - 0");
		}
	}
}

class ThreadDemo_Example11{
	public static void main(String[] args) throws InterruptedException{
		MyThread_Example11.mnName = Thread.currentThread();
		MyThread_Example11 obj = new MyThread_Example11();
		obj.start();

		for(int i=0; i<10; i++){
			obj.join(1000);
			/*
			 * Waits at most {@code millis} milliseconds for this thread to
			 * die. A timeout of {@code 0} means to wait forever.
			 *
			 * This implementation uses a loop of {@code this.wait} calls
			 * conditioned on {@code this.isAlive}. As a thread terminates the
			 * {@code this.notifyAll} method is invoked. It is recommended that
			 * applications not use {@code wait}, {@code notify}, or
			 * {@code notifyAll} on {@code Thread} instances.
			 *
			 * @param  millis
			 * 	the time to wait in milliseconds
			 *
			 * @throws  IllegalArgumentException
			 * 	if the value of {@code millis} is negative
			 *
			 * @throws  InterruptedException
			 *           if any thread has interrupted the current thread. The
			 *           <i>interrupted status</i> of the current thread is
			 *           cleared when this exception is thrown.
			 */
                        System.out.println("In main");
                }
	}
}
