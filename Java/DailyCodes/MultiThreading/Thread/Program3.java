/*
 * Example - 3
 */

class Demo_Example3 extends Thread{
	public void run(){
		System.out.println("Demo_Example3 : " + Thread.currentThread().getName());
	}
}

class MyThread_Example3 extends Thread{
	public void run(){
		System.out.println("MyThread_Example3 : " + Thread.currentThread().getName());
		Demo_Example3 obj = new Demo_Example3();
		obj.start();
	}
}

class ThreadDemo_Example3{
	public static void main(String[] args){
		System.out.println("ThreadDemo_Example3 : " + Thread.currentThread().getName());
		MyThread_Example3 obj = new MyThread_Example3();
		obj.start();
	}
}
