/*
 * Example - 2
 *
 * ExecutorService newCachedThreadPool();
 */

import java.util.concurrent.*;
class MyThread_Example2 implements Runnable{
	int num;
	MyThread_Example2(int num){
		this.num = num;
	}
	public void run(){
		System.out.println(Thread.currentThread() + " Start Thread : " + num);
		dailyTask();
		System.out.println(Thread.currentThread() + " End Thread : " + num);
	}
	void dailyTask(){
		try{
			Thread.sleep(8000);
		}catch(InterruptedException obj){
			obj.toString();
		}
	}
}

class ThreadPoolDemo_Example2{
	public static void main(String[] args){
		ExecutorService ser = Executors.newCachedThreadPool();
		for(int i=1; i<=6; i++){
			MyThread_Example2 obj = new MyThread_Example2(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}
