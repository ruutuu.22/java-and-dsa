/*
 * Example - 2
 * Real-time example for Nested Switch
 */

class Example2{
	public static void main(String[] rutu){
		System.out.println("Indian Cricket Team");
		String str = "Womens Team";
		switch(str){
			case "Mens Team":
				{
					String str1 = "Wicketkeeper";
					switch(str1){
						case "Captain":
							System.out.println("Rohit Sharma");
							break;

						case "Batsmen":
							System.out.println("Virat Kohli");
							break;

						case "Wicketkeeper":
							System.out.println("Rishabh Pant");
							break;

						case "Bowler":
							System.out.println("Jasprit Bumrah");
							break;
					}
				}
				break;

			case "Womens Team":
				{
					String str1 = "Wicketkeeper";
					switch(str1){
						case "Captain":
							System.out.println("Harmanpreet Kaur");
							break;

						case "Batsmen":
							System.out.println("Smruti Mandhana");
							break;

						case "Wicketkeeper":
							System.out.println("Yashtika Bhatia");
							break;

						case "Bowler":
							System.out.println("Deepti Sharma");
							break;
					}
				}
				break;
		}
	}
}
