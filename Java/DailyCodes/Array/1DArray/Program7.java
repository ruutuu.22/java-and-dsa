/*
 * Example - 7
 * Integer Cache
 */

class Example7{
	public static void main(String[] args){
		int x = 10;
		int y = 10;
		Integer z = 10;
		Integer a = new Integer(10);			//warning

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(z));
		System.out.println(System.identityHashCode(a));
	}
}	
