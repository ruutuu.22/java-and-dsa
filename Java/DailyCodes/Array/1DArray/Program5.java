/*
 * Example - 5
 * Taking array elements as input and printing thecount of even elements
 */

import java.util.*;
class Example5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int [size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		
		int count = 0;
		System.out.print("Even array element count is : ");
		for(int i=0; i<arr.length; i++){
			if(arr[i]%2 == 0){
				count++;
			}
		}
		System.out.println(count);
	}
}

