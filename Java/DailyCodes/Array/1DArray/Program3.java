/*
 * Example - 3
 * Taking array elements as input
 */

import java.util.*;
class Example3{
	public static void main(String[] args){
		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Array elements are : ");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}
}

