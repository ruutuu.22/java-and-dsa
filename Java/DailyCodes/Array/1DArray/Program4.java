/*
 * Example - 4
 * Sum of array elements
 */

import java.util.*;
class Example4{
	public static void main(String[] args){
		int arr[] = new int[5];
		int sum = 0;
		Scanner sc = new Scanner(System.in);
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
			sum = sum + arr[i];
		}
		System.out.println("Sum of array elements is " + sum);
	}
}

