/*
 * Example - 5
 */

import java.io.*;
class Example5{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arr[][] = new int[3][];
		arr[0] = new int[3];
		arr[1] = new int[2];
		arr[2] = new int[1];;

		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		for(int[] arr1 : arr){
			for(int x : arr1){
				System.out.print(x + " ");
			}
			System.out.println();
		}
	}
}

