/*
 * Example - 3
 */

import java.util.*;
class Example3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rows : ");
		int rows = sc.nextInt();
		System.out.println("Enter columns : ");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				System.out.print("arr[" + i + "][" + j + "] = ");
				arr[i][j] = sc.nextInt();
			}
		}

		System.out.println("2D Array elements are : ");
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}
}	
