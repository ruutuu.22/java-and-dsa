/*
 * Example - 1
 */

import java.util.*;
class Example1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter planes : ");
		int planes = sc.nextInt();
		System.out.println("Enter rows : ");
		int rows = sc.nextInt();
		System.out.println("Enter columns : ");
		int cols = sc.nextInt();

		int arr[][][] = new int[planes][rows][cols];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				for(int k=0; k<arr[i][j].length ; k++){
					System.out.print("arr[" + i + "][" + j + "][" + k + "] = ");
					arr[i][j][k] = sc.nextInt();
				}
			}
		}

		System.out.println("3D Array elements are : ");
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				for(int k=0; k<arr[i][j].length; k++){
					System.out.print(arr[i][j][k] + " ");
				}
				System.out.println();
			}
			System.out.println();
		}
	}
}	
