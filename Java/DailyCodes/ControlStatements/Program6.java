/*
 * Divisible by 4
 */

class Divisibility{
	public static void main(String[] rutu){
		int x = 20;

		if(x % 4 == 0){
			System.out.println(x + " is Divisible by 4");
		}else{
			System.out.println(x + " is Not Divisible by 4");
		}
	}
}
