/*
 * units <= 100 := price per unit is 1
 * units >= 100 := price per unit is 2
 */

class ElectricityPB{
	public static void main(String[] rutu){
		int x = 200;
		int price;

		if(x <= 100){
			price = x;
		}else{
			price = (x - 100)*2 + 100;
		}
		System.out.println("Price = " + price);
	}
}
