/*
 * Example - 2
 */

import java.io.*;
class Example2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter player name : ");
		String pName = br.readLine();

		System.out.println("Enter jersey number : ");
		int jerNo = Integer.parseInt(br.readLine());

		System.out.println("Enter average : ");
		float avg = Float.parseFloat(br.readLine());

		System.out.println("Player's Name : " + pName);
		System.out.println("Jersey Number : " + jerNo);
		System.out.println("Average : " + avg);
	}
}
/*
 * Other parse methods are:
 * parseInt
 * parseFloat
 * parseShort
 * parseLong
 * parseBoolean
 * parseDouble
 * parseByte
 */

