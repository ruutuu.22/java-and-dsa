/*
 * Example - 1
 * BufferedReader Class is included in java.io package
 */

import java .io.*;
class Example1{
	public static void main(String[] args) throws IOException{
		/*
		 * IOException class is included in java.io package
		 * IOException error is only shown when the java.io package is already imported,
		 * but if package is not imported it will only give error to the imported class name.
		 */

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Batsman Name : ");
		String batsmanName = br.readLine();

		System.out.println("Enter Bowler Name : ");
		String bowlerName = br.readLine();

		System.out.println("Batsman's name is " + batsmanName);
		System.out.println("Bowler's name is " + bowlerName);
	}
}
