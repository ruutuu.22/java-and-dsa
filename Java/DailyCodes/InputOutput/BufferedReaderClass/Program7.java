/*
 * Example - 7
 * StringTokenizer()
 */

import java.io.*;
import java.util.*;
class Example7{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Match info, MOM and Runs : ");
		String info = br.readLine();

		StringTokenizer obj = new StringTokenizer(info , " ");

		String token1 = obj.nextToken();
		String token2 = obj.nextToken();
		int token3 = Integer.parseInt(obj.nextToken());

		System.out.println("Match Info = " + token1);
		System.out.println("MOM = " + token2);
		System.out.println("Runs = " + token3);
	}
}
