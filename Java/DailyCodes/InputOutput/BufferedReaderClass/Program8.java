/*
 * Example - 8
 * Real Time Example
 */

import java.io.*;
import java.util.*;

class Example8{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Player Name, Grade, Jersey Number, Batting Average : ");

		String batsman = br.readLine();
		StringTokenizer obj = new StringTokenizer(batsman , ",");
		String Token1 = obj.nextToken();
		String Token2 = obj.nextToken();
		char grade = Token2.charAt(0);
		int jerNo = Integer.parseInt(obj.nextToken());
		float avg = Float.parseFloat(obj.nextToken());

		System.out.println("Player Name : " + Token1);
		System.out.println("Grade : " + grade);
		System.out.println("Jersey Number : " + jerNo);
		System.out.println("Batting Average : " + avg);
	}
}
