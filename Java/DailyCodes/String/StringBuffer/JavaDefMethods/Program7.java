/*
 * Method - 7
 *
 * Method => public synchronized int indexOf(String str, int fromIndex);
 *
 * Description => 
 * 	-Finds the first instance of a String in this StringBuffer, starting at a given index.
 * 	-If the starting is less than 0, the search starts at the beginning of this string.
 * 	-If the start index is greater than the length ofthis STring, or the substring is not found, -1 is returned.
 *
 * Parameters => String(str String to find), Integer(fromIndex index to start the search).
 *
 * Return Type => Integer(location(base 0) of the String or -1 if not found).
 */

class Method7{
	public static void main(String[] args){
		StringBuffer str1 = new StringBuffer("Rutuparn");

		System.out.println(str1.indexOf("u", 2));
	}
}
