/*
 * Method - 4
 *
 * Method => public synchronized StringBuffer reverse();
 *
 * Description => 
 * 	-Reverse the characters in the StringBuffer
 * 	-The same sequence of characters exists, but in the reverse index ordering.
 *
 * Parameters => No parameters
 *
 * Return Type => StringBuffer(this StringBuffer).
 */

class Method4{
	public static void main(String[] args){
		String str = "Rutuparn";
		StringBuffer sb = new StringBuffer(str);

		str = sb.reverse().toString();
		System.out.println(str);
	}
}
