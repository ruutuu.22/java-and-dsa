/*
 * Method - 5
 *
 * Method => public String toString();
 *
 * Description => 
 * 	-Convert this StringBuffer to a String.
 * 	-Note that the result is a copy and that future modifications to this buffer do not affect the String.
 *
 * Parameters => No parameters
 *
 * Return Type => String(the characters in this StringBuffer).
 */

class Method5{
	public static void main(String[] args){
		StringBuffer str1 = new StringBuffer("Rutuparn Rajesh ");
		String str2 = "Sadvelkar ";

		String str3 = str1.toString();

		String str4 = str3.concat(str2);

		System.out.println(str4);
	}
}
