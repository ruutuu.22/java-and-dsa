/*
 * Example - 6
 */

class Example6{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		//String str2 = "Sadvelkar";
		String str2 = new String("Sadvelkar");

		StringBuffer str3 = new StringBuffer("Core2Web");
		//String str4 = str3.append(str1);
		//Above instruction has an error
		//str4 => String
		//str3 => StringBuffer
		//append() => method of StringBuffer class
		//str1 => String

		//String str4 = str1.append(str3);
		//Above instruction has an error
		//str4 => String
		//str1 => String
		//append() => method of StringBuffer class
		//str3 => StringBuffer

		StringBuffer str4 = str3.append(str1);
		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);

		//String str4 = str1.concat(str3);
		//Above instruction has an error
                //str4 => String
                //str1 => String
                //concat() => method of String class
                //str3 => StringBuffer
	}
}
