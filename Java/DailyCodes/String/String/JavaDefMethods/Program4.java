/*
 * Method - 4
 *
 * Method => public int compareTo(String str2);
 *
 * Description => It compares the str1 and str2 (case sensitive), if both the strings are equal, it returns 0 otherwise returns the comparison.
 *
 *Example => str1.compareTo(str2)
 *
 * Parameters => String(second string)
 *
 * Return type => Integer
 */

class Method4{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		String str2 = "Rutupran";
		System.out.println(str1.compareTo(str2));
	}
}
