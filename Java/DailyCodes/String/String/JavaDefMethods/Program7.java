/*
 * Method - 7
 *
 * Method => public boolean equalsIgnoreCase(String anotherString);
 *
 * Description => Compares a string to this string ignoring case.
 *
 * Parameters => String(str2)
 *
 * Return type => Boolean
 */

class Method7{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		String str2 = new String("rutuparn");
		System.out.println(str1.equalsIgnoreCase(str2));
	}
}
