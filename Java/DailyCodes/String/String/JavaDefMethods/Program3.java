/*
 * Method - 3
 *
 * Method => public char charAt(int index);
 * 
 * Description => It returns the character located at specified index within the given String
 * 
 * Parameters => integer(index)
 * 
 * Return type => character
 */

class Method3{
	public static void main(String[] args){
		String str = "Core2Web";
		System.out.println(str.charAt(4));
		System.out.println(str.charAt(0));
		System.out.println(str.charAt(8));
	}
}
