/*
 * Method - 15
 *
 * Method => public String toLowerCase();
 *
 * Description => It lowercases to the string.
 *
 * Example => str.toLowerCase();
 *
 * Parameter => no arguements.
 *
 *Return type => String
 */

class Method15{
	public static void main(String[] args){
		String str = "Hello World!";
		System.out.println(str.toLowerCase());
	}
}

