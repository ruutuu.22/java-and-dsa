/*
 * Method - 18
 *
 * Method => public String[] split(String delimiter);
 *
 * Description => Splits this string around matches of regular expressions.
 *
 * Parameter => delimiter(pattern to match)
 *
 *Return type => String[] (array of split strings)
 */

class Method18{
	public static void main(String[] args){
		String str = "Rutuparn Rajesh Sadvelkar";
		String strResult[] = str.split(" "); 
		for(int i=0; i<strResult.length; i++){
			System.out.println(strResult[i]);
		}
	}
}

