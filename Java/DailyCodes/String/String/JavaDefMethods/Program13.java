/*
 * Method - 13
 *
 * Method => public String substring(int index);
 *
 * Description => creates a substring of the given String starting at a specified index and ending at the end of given String
 *
 * Parameters => integer(index of the string)
 *
 *Return type => String
 */

class Method13{
	public static void main(String[] args){
		String str = "Hello World!";
		System.out.println(str.substring(5));;
	}
}

