/*
 * Method - 9
 *
 * Method => public boolean endsWith(String suffix);
 *
 * Description => 
 * 1. Predicate which determines if the given String ends with given suffix.
 * 2. If the suffix is an empty String, true is returned.
 * 3. Throws NullPointerException if suffix is null.
 *
 * Parameters => prefix string to compare.
 *
 * Return type => Boolean
 */

class Method9{
	public static void main(String[] args){
		String str = "Rutuparn Rajesh Sadvelkar";
		System.out.println(str.endsWith("kar"));
	}
}

