/*
 * Method - 2
 *
 * Method => public int length();
 *
 * Description => It return the number of characters contained in given string.
 *
 * Parameters => No parameters
 *
 * Return Type => Integer
 */

class Method2{
	public static void main(String[] args){
		String str1 = "Core2Web";
		System.out.println(str1.length());
		//System.out.println(str1[0]);			//error : array rerquired but string found
	}
}
