/*
 * Example - 1
 *
 * In Java, LinkedList has index because it comes under List Interface
 */

import java.util.*;
class Example1 extends LinkedList{
	public static void main(String[] args){
		Example1 ll = new Example1();
		
		ll.add(20);
		System.out.println(ll);

		ll.addFirst(10);
		System.out.println(ll);

		ll.addLast(30);
		System.out.println(ll);

		ll.add(2, 25);
		System.out.println(ll);

		System.out.println(ll.getFirst());
		System.out.println(ll.getLast());

		//System.out.println(ll.node(0));

		System.out.println(ll.removeFirst());
		System.out.println(ll.removeLast());
		System.out.println(ll);
	}
}
