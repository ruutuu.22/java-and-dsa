/*
 * Example - 1
 *
 * ArrayList
 * 	>Array is of fixed size but Array List is of dynamic size.
 * 	>Stores different types of data unlike Array being homogeneous.
 * 	>built in methods available.
 *
 * 	E => Element
 */

import java.util.*;

class Example1 extends ArrayList{
	public static void main(String[] args){
		Example1 al = new Example1();

		//public void add(E);
		System.out.println("----------add(E)----------");
		al.add(10);
		al.add(20.5f);
		al.add("Rutuparn");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);

		//public int size();
		System.out.println("----------size()----------");
		System.out.println(al.size());

		//public boolean contains(java.lang.Object);
		System.out.println("----------contains(java.lang.Object)----------");
		System.out.println("Rutuparn => " + al.contains("Rutuparn"));
		System.out.println("30 => " + al.contains(30));

		//public int indexOf(java.lang.Object);
		System.out.println("----------indexOf(java.lang.Object)----------");
		System.out.println(al.indexOf("Rutuparn"));

		//public int lastIndexOf(java.lang.Object);
                System.out.println("----------lastIndexOf(java.lang.Object)----------");
                System.out.println(al.lastIndexOf(10));

		//public E get(int);
		System.out.println("----------get(int)----------");
		System.out.println(al.get(3));

		//public E set(int, E);
                System.out.println("----------set(int, E)----------");
                System.out.println(al.set(3, "Sadvelkar"));

		//public boolean addAll(java.util.Collection<? extends E>);
		System.out.println("----------addAll(java.util.Collection<? extends E>----------");
		ArrayList al2  = new ArrayList();
		al2.add("AAA");
		al2.add("BBB");
		al.addAll(al2);
		System.out.println(al);

		//public boolean addAll(int, java.util.Collection<? extends E>);
		System.out.println("----------addAll(int, java.util.Collection<? extends E>)----------");
		al.addAll(3, al2);
		System.out.println(al);
		
		//protected void removeRange(int, int);
		System.out.println("----------protected void removeRange(int, int);----------");
		al.removeRange(3,6);
		System.out.println(al);
		
		//public boolean remove(java.lang.Object);
		System.out.println("----------public boolean remove(java.lang.Object)----------");
		al.remove("Rutuparn");
		System.out.println(al);
		
		//public E remove(int);
		System.out.println("----------E remove(int)----------");
		System.out.println(al.remove(3));
		System.out.println(al);
		
		//public java.lang.Object[] toArray();
		System.out.println("----------java.lang.Object[] toArray()----------");
		Object arr[] = al.toArray();
		for(var x:arr){
			System.out.println(x);	
		}

		//public void clear();
		al.clear();
		System.out.println(al);
	}
}

