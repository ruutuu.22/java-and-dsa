/*
 * Example - 2
 *
 * UserDefined Data
 * Here toString() should be overrided,
 * because SOP(al) goes as SOP(al.toString()),
 * if not overrided gives address.
 */

import java.util.*;
class ArrayListDemo_Example2{
	int jerNo = 0;
	String pName = null;

	ArrayListDemo_Example2(int jerNo, String pName){
		this.jerNo = jerNo;
		this.pName = pName;
	}

	public String toString(){
		return "{ " + jerNo + " : " + pName + " }";
	}
}

class Example2{
	public static void main(String[] args){
		ArrayList al = new ArrayList();
		al.add(new ArrayListDemo_Example2(18, "Virat"));
		al.add(new ArrayListDemo_Example2(45, "Rohit"));
		al.add(new ArrayListDemo_Example2(7, "MSD"));

		System.out.println(al);

		al.add(1, new ArrayListDemo_Example2(33, "Hardik"));

		System.out.println(al);
	}
}
